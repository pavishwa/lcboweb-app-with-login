package sheridan;
import static org.junit.Assert.*;
import org.junit.Test;
public class LoginValidatorTest {

	@Test
	public void testIsValidLoginSixCharRegulars( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Vishwa123" ) );
	}
	@Test
	public void testIsValidLoginSixCharException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "v12is" ) );
	}
	@Test
	public void testIsValidLoginSixCharBoundryIn( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "vis12" ) );
	}
	@Test
	public void testIsValidLoginSixCharBoundryOut( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Vishwa12345" ) );
	}
	@Test
	public void testIsValidLoginNotStartsWithNumRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Vis123" ) );
	}
	@Test
	public void testIsValidLoginNotStartsWithNumBoundryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "V1shwa" ) );
	}
	@Test
	public void testIsValidLoginNotStartsWithNumBoundryOut( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "V1234567" ) );
	}
	@Test
	public void testIsValidLoginNotStartsWithNumException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "123Vishwa" ) );
	}
}
